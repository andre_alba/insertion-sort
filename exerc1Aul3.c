#include <stdio.h>
#include <stdlib.h>
void imprime(int*, int);
void troca(int*, int);
void insertionSort(int*,int,int);

int main(int argc, char **argv)
{
	int tam, *in,i,ini,aux;
	
	//Lê o o mero de valores que serão recebidos.
	scanf("%d",&tam);
	
	//Aloca o tamanho do vetor que sera usado
	in = malloc(tam*sizeof(int));
	if (in == NULL) {
		perror("Error");
		return EXIT_FAILURE;
		}
    
    //Coloca todos os valores do arquivo de entrada no ponteiro in.
    for(i=0;i<tam;i++){
		
		scanf("%d",&in[i]);
		
		}
	printf("Tamanho do vetor: %d\n",tam);
	
	//Ini e o indice inicial do vetor para começar a organizar
	
	ini=0;
	
	//Aux recebe o ultimo indice do vetor a ser organizado.
	//aux=5;
	aux=tam; //passa o tamanho para o indice final para organizar todo o vetor 
	
	
	
	imprime(in,tam);
	insertionSort(in,ini,aux);	
	imprime(in,tam);
	
	return 0;

}

void imprime(int *in, int tam){
	int i;
	printf("{");
	for(i=0;i<tam-1;i++){
					
				
			printf("%d,",in[i]);
			
		
		}
	printf("%d}\n",in[i]);

	
}

void troca(int *in, int ind){
	
	int aux;
	
		aux=in[ind];
		in[ind]=in[ind-1];
		in[ind-1]=aux;
	
}


void insertionSort(int *in, int ini, int fin){
		
		int j,i;
		
		
		
		for(i=ini+1;i<fin;i++){
				j=i;
			while((j>0) && (in[j]<in[j-1])){
				
				troca(in, j);
				j--;
				}	
				
			}
	
}
