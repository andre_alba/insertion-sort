#include <stdio.h>
#include <stdlib.h>
void imprime(int*, int);
void troca(int *, int);
void bubbleSort(int *,int);

int main(int argc, char **argv)
{
	int tam, *i,j;
	
	scanf("%d",&tam);
	
	i = malloc(tam*sizeof(int));
	if (i == NULL) {
		perror("Error");
		return EXIT_FAILURE;
		}
    
    for(j=0;j<tam;j++){
		
		scanf("%d",&i[j]);
		
		}
	printf("Tamanho do vetor: %d\n",tam);
	tam--;
	
	
	imprime(i,tam);
	bubbleSort(i,tam);	
	imprime(i,tam);
	
	return 0;

}

void imprime(int *i, int tam){
	int j;
	printf("{");
	for(j=0;j<tam;j++){
					
				
			printf("%d,",i[j]);
			
		
		}
	printf("%d}\n",i[tam]);

	
}

void troca(int *i, int k){
	
	int aux;
	
		aux=i[k];
		i[k]=i[k-1];
		i[k-1]=aux;
	
}


void bubbleSort(int *i, int tam){
	int l,m;
	
	
	for(l=0;l<tam;l++){
		for(m=tam;m>l;m--){
			
			//printf("%d, ",m);
			if (i[m]<i[m-1]){
			
				troca(i,m);
				
				}
			}
		
		}
	
	
}
